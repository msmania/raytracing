#pragma once

struct material {
  virtual bool scatter(const ray& ray_in,
                       const hit_record& record,
                       vec3& attenuation,
                       ray& ray_scattered) const = 0;
};

struct lambertian : material {
  vec3 albedo_;
  lambertian(const vec3& a);
  virtual bool scatter(const ray& ray_in,
                       const hit_record& rec,
                       vec3& attenuation,
                       ray& ray_scattered) const;
};

struct metal : material {
  vec3 albedo_;
  float fuzz_;
  metal(const vec3& a, float fuzz);
  virtual bool scatter(const ray& ray_in,
                       const hit_record& rec,
                       vec3& attenuation,
                       ray& ray_scattered) const;
};

struct dielectric : material {
  float ref_idx_;
  dielectric(float r);
  virtual bool scatter(const ray& ray_in,
                       const hit_record& rec,
                       vec3& attenuation,
                       ray& ray_scattered) const;
};
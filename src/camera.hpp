#pragma once

struct camera {
  vec3 origin_, lower_left_corner_, horizontal_, vertical_;
  camera(vec3 lookfrom,
         vec3 lookat,
         vec3 vup,
         float vfov,
         float aspect);
  ray get_ray(float u, float v) const;
};
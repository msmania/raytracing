#include <iostream>
#include <vector>
#include <memory>
#include <random>
#include <cfloat>
#include <math.h>
#include "vec3.hpp"
#include "ray.hpp"
#include "hittable.hpp"
#include "sphere.hpp"
#include "hittable_list.hpp"
#include "camera.hpp"
#include "material.hpp"

std::default_random_engine generator;
std::uniform_real_distribution<double> distribution(0.0,1.0);

vec3 color(const ray& r, const hittable &world, int depth) {
  hit_record rec;
  if (world.hit(r, .001f, FLT_MAX, rec)) {
    vec3 attenuation;
    ray scattered;
    if (depth < 50
        && rec.material_->scatter(r, rec, attenuation, scattered))
      return attenuation * color(scattered, world, depth + 1);
    else
      return vec3();
  }
  vec3 direction = unit_vector(r.direction());
  float t = .5f * (direction.y() + 1);
  return t * vec3(.5f, .7f, 1.0f)
         + (1.0f - t) * vec3(1.0f, 1.0f, 1.0f);
}

std::unique_ptr<hittable> generateRandomScene() {
  auto scene = std::make_unique<hittable_list>();
  scene->emplace_back(std::make_unique<sphere>(
    vec3(.0f, -1000.f, .0f), 1000.f,
    new lambertian(vec3(.5f, .5f, .5f))));
  constexpr int n = 11;
  for (int a = -n; a < n; ++a) {
    for (int b = -n; b < n; ++b) {
      vec3 center(.9f * distribution(generator) + a,
                  .2f,
                  .9f * distribution(generator) + b);
      if ((center - vec3(4.f, .2f, .0f)).length() > .9f) {
        const float choose_material = distribution(generator);
        if (choose_material < .8f) {
          float r = distribution(generator) * distribution(generator),
                g = distribution(generator) * distribution(generator),
                b = distribution(generator) * distribution(generator);
          scene->emplace_back(std::make_unique<sphere>(
            center, .2f,
            new lambertian(vec3(r, g, b))));
        }
        else if (choose_material < .95f) {
          float r = .5f * (1.f + distribution(generator)),
                g = .5f * (1.f + distribution(generator)),
                b = .5f * (1.f + distribution(generator)),
                f = .5f * distribution(generator);
          scene->emplace_back(std::make_unique<sphere>(
            center, .2f,
            new metal(vec3(r, g, b), f)));
        }
        else {
          scene->emplace_back(std::make_unique<sphere>(
            center, .2f,
            new dielectric(1.5f)));
        }
      }
    }
  }
  scene->emplace_back(std::make_unique<sphere>(
    vec3(0.f, 1.f, 0.f), 1.f,
    new dielectric(1.5f)));
  scene->emplace_back(std::make_unique<sphere>(
    vec3(-4.f, 1.f, 0.f), 1.f,
    new lambertian(vec3(.4f, .2f, .1f))));
  scene->emplace_back(std::make_unique<sphere>(
    vec3(4.f, 1.f, 0.f), 1.f,
    new metal(vec3(.7f, .6f, .5f), .0f)));
  return scene;
}

void generatePPM(std::ostream& os) {
  int nx = 800, ny = 600, ns = 100;
  os << "P3\n" << nx << ' ' << ny << "\n255\n";
  camera cam(vec3(9.f, 1.7f, 2.5f),
             vec3(.8f, 1.f, 0.f),
             vec3(0.f, 1.f, 0.f),
             35.f,
             static_cast<float>(nx) / ny);
  auto world = generateRandomScene();
  for (int j = ny - 1; j >= 0; --j) {
    for (int i = 0; i < nx; ++i) {
      vec3 col;
      for (int k = 0; k < ns; ++k) {
        float u = static_cast<float>(i + distribution(generator)) / nx;
        float v = static_cast<float>(j + distribution(generator)) / ny;
        col += color(cam.get_ray(u, v), *world.get(), 0);
      }
      col = vec3(sqrt(col.r() / ns),
                 sqrt(col.g() / ns),
                 sqrt(col.b() / ns));
      int ir = static_cast<int>(255.99f * col[0]);
      int ig = static_cast<int>(255.99f * col[1]);
      int ib = static_cast<int>(255.99f * col[2]);
      os << ir << ' ' << ig << ' ' << ib << '\n';
    }
  }
}

int main() {
  generatePPM(std::cout);
  return 0;
}

#pragma once

struct material;

struct hit_record {
  float t_{};
  vec3 p_;
  vec3 normal_;
  material *material_{};
};

struct hittable {
  virtual ~hittable() {}
  virtual bool hit(const ray& r,
                   float t_min,
                   float t_max,
                   hit_record& rec) const = 0;
};
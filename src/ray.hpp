#pragma once

struct ray {
  vec3 A_, B_;
  ray();
  ray(const vec3& origin, const vec3& direction);
  vec3 origin() const;
  vec3 direction() const;
  vec3 operator[](float t) const;
};
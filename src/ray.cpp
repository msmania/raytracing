#include <iostream>
#include "vec3.hpp"
#include "ray.hpp"

ray::ray()
{}
ray::ray(const vec3& origin, const vec3& direction)
  : A_(origin), B_(direction)
{}
vec3 ray::origin() const { return A_; }
vec3 ray::direction() const { return B_; }
vec3 ray::operator[](float t) const { return A_ + t * B_; }

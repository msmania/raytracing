#include <random>
#include <iostream>
#include "vec3.hpp"
#include "ray.hpp"
#include "hittable.hpp"
#include "material.hpp"

extern std::default_random_engine generator;
extern std::uniform_real_distribution<double> distribution;

vec3 random_unit_sphere() {
  vec3 p;
  do {
    p = vec3(distribution(generator) * 2.0f - 1.0f,
             distribution(generator) * 2.0f - 1.0f,
             distribution(generator) * 2.0f - 1.0f);
  } while (p.squared_length() >= 1.0f);
  return p;
}

lambertian::lambertian(const vec3& a)
  : albedo_(a)
{}

bool lambertian::scatter(const ray& ray_in,
                         const hit_record& rec,
                         vec3& attenuation,
                         ray& ray_scattered) const {
  vec3 target = rec.p_ + rec.normal_ + random_unit_sphere();
  ray_scattered = ray(rec.p_, target - rec.p_);
  attenuation = albedo_;
  return true;
}  

metal::metal(const vec3& a, float fuzz)
  : albedo_(a), fuzz_(fuzz < 1 ? fuzz : 1.0f)
{}

vec3 reflect(const vec3& v, const vec3& n) {
  return v - 2.0f * dot(v, n) * n;
}

bool metal::scatter(const ray& ray_in,
                    const hit_record& rec,
                    vec3& attenuation,
                    ray& ray_scattered) const {
  vec3 reflected = reflect(unit_vector(ray_in.direction()), rec.normal_);
  ray_scattered = ray(rec.p_, reflected + fuzz_ * random_unit_sphere());
  attenuation = albedo_;
  return dot(reflected, rec.normal_) > 0;
}  

dielectric::dielectric(float r)
  : ref_idx_(r)
{}

bool refract(const vec3& v,
             const vec3& n,
             float ni_over_nt,
             vec3& refracted) {
  vec3 uv = unit_vector(v);
  float dt = dot(uv, n),
        discriminant = 1.0 - ni_over_nt * ni_over_nt * (1 - dt * dt);
  if (discriminant > 0) {
    refracted = ni_over_nt * (uv - n * dt) - n * sqrt(discriminant);
    return true;
  }
  return false;
}

float schlick(float cosine, float ref_idx) {
  float r0 = (1.0f - ref_idx) / (1 + ref_idx);
  r0 = r0 * r0;
  return r0 + (1.0 - r0) * pow((1-cosine), 5);
}

bool dielectric::scatter(const ray& ray_in,
                         const hit_record& rec,
                         vec3& attenuation,
                         ray& ray_scattered) const {
  attenuation = vec3(1.0f, 1.0f, 1.0f);
  vec3 outward_normal,
       reflected = reflect(ray_in.direction(), rec.normal_);
  float ni_over_nt, cosine, reflect_prob;
  if (dot(ray_in.direction(), rec.normal_) > 0) {
    outward_normal = -rec.normal_;
    ni_over_nt = ref_idx_;
    cosine = ref_idx_ * dot(ray_in.direction(), rec.normal_)
             / ray_in.direction().length();
  }
  else {
    outward_normal = rec.normal_;
    ni_over_nt = 1.0f / ref_idx_;
    cosine = -dot(ray_in.direction(), rec.normal_)
             / ray_in.direction().length();
  }
  vec3 refracted;
  if (refract(ray_in.direction(), outward_normal, ni_over_nt, refracted)) {
    reflect_prob = schlick(cosine, ref_idx_);
  }
  else {
    reflect_prob = 1.0f;
  }
  ray_scattered = ray(rec.p_,
                      distribution(generator) < reflect_prob
                      ? reflected : refracted);
  return true;
}
#include <iostream>
#include <vector>
#include <memory>
#include <math.h>
#include "vec3.hpp"
#include "ray.hpp"
#include "hittable.hpp"
#include "hittable_list.hpp"

hittable_list::hittable_list()
{}

void hittable_list::emplace_back(std::unique_ptr<hittable> &&item) {
  list_.emplace_back(std::move(item));
}

bool hittable_list::hit(const ray& r,
                        float t_min,
                        float t_max,
                        hit_record& rec) const {
  hit_record record_temp;
  bool hit_anything = false;
  double closest_so_far = t_max;
  for (const auto& it : list_) {
    if (it->hit(r, t_min, closest_so_far, record_temp)) {
      hit_anything = true;
      closest_so_far = record_temp.t_;
      rec = record_temp;
    }
  }
  return hit_anything;
}
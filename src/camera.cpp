#include <iostream>
#include <math.h>
#include "vec3.hpp"
#include "ray.hpp"
#include "camera.hpp"

camera::camera(vec3 lookfrom,
               vec3 lookat,
               vec3 vup,
               float vfov,
               float aspect) {
  const float theta = vfov * M_PI / 180.0f,
              half_height = tan(theta / 2),
              half_width = half_height * aspect;
  origin_ = lookfrom;
  vec3 w = unit_vector(lookfrom - lookat),
       u = unit_vector(cross(vup, w)),
       v = cross(w, u);
  lower_left_corner_ = lookfrom - half_width * u - half_height * v - w;
  horizontal_ = 2.0f * half_width * u;
  vertical_ = 2.0f * half_height * v;
}

ray camera::get_ray(float u, float v) const {
  return ray(origin_,
             lower_left_corner_ + u * horizontal_ + v * vertical_ - origin_);
}

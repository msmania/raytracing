#include <math.h>
#include <iostream>
#include "vec3.hpp"

vec3::vec3()
  : e_{}
{}

vec3::vec3(float e0, float e1, float e2)
  : e_{e0, e1, e2}
{}

float vec3::x() const { return e_[0]; }
float vec3::y() const { return e_[1]; }
float vec3::z() const { return e_[2]; }
float vec3::r() const { return e_[0]; }
float vec3::g() const { return e_[1]; }
float vec3::b() const { return e_[2]; }

const vec3& vec3::operator+() const { return *this; }
vec3 vec3::operator-() const {
  return vec3{-e_[0], -e_[1], -e_[2]};
}

float vec3::operator[](int i) const { return e_[i]; }
float& vec3::operator[](int i) { return e_[i]; }

vec3& vec3::operator+=(const vec3& other) {
  e_[0] += other.e_[0];
  e_[1] += other.e_[1];
  e_[2] += other.e_[2];
  return *this;
}
vec3& vec3::operator-=(const vec3& other) {
  e_[0] -= other.e_[0];
  e_[1] -= other.e_[1];
  e_[2] -= other.e_[2];
  return *this;
}
vec3& vec3::operator*=(const vec3& other) {
  e_[0] *= other.e_[0];
  e_[1] *= other.e_[1];
  e_[2] *= other.e_[2];
  return *this;
}
vec3& vec3::operator/=(const vec3& other) {
  e_[0] /= other.e_[0];
  e_[1] /= other.e_[1];
  e_[2] /= other.e_[2];
  return *this;
}

vec3& vec3::operator+=(float t) {
  e_[0] += t;
  e_[1] += t;
  e_[2] += t;
  return *this;
}
vec3& vec3::operator-=(float t) {
  e_[0] -= t;
  e_[1] -= t;
  e_[2] -= t;
  return *this;
}
vec3& vec3::operator*=(float t) {
  e_[0] *= t;
  e_[1] *= t;
  e_[2] *= t;
  return *this;
}
vec3& vec3::operator/=(float t) {
  auto k = 1.0f / t;
  e_[0] *= k;
  e_[1] *= k;
  e_[2] *= k;
  return *this;
}

float vec3::length() const {
  return sqrtf(e_[0] * e_[0]
               + e_[1] * e_[1]
               + e_[2] * e_[2]);
}
float vec3::squared_length() const {
  return e_[0] * e_[0]
         + e_[1] * e_[1]
         + e_[2] * e_[2];

}

void vec3::normalize() {
  auto k = 1.0f / length();
  e_[0] *= k;
  e_[1] *= k;
  e_[2] *= k;
}

std::istream& operator>>(std::istream& is, vec3& t) {
  return is >> t.e_[0] >> t.e_[1] >> t.e_[2];
}
std::ostream& operator<<(std::ostream& os, const vec3& t) {
  return os << t.e_[0] << ' ' << t.e_[1] << ' ' << t.e_[2];
}

vec3 operator+(const vec3& v1, const vec3& v2) {
  return vec3(v1.e_[0] + v2.e_[0],
              v1.e_[1] + v2.e_[1],
              v1.e_[2] + v2.e_[2]);
}
vec3 operator-(const vec3& v1, const vec3& v2) {
  return vec3(v1.e_[0] - v2.e_[0],
              v1.e_[1] - v2.e_[1],
              v1.e_[2] - v2.e_[2]);
}
vec3 operator*(const vec3& v1, const vec3& v2) {
  return vec3(v1.e_[0] * v2.e_[0],
              v1.e_[1] * v2.e_[1],
              v1.e_[2] * v2.e_[2]);
}
vec3 operator/(const vec3& v1, const vec3& v2) {
  return vec3(v1.e_[0] / v2.e_[0],
              v1.e_[1] / v2.e_[1],
              v1.e_[2] / v2.e_[2]);
}

vec3 operator*(float t, const vec3& v) {
  return vec3(t * v.e_[0],
              t * v.e_[1],
              t * v.e_[2]);
}
vec3 operator*(const vec3& v, float t) {
  return vec3(t * v.e_[0],
              t * v.e_[1],
              t * v.e_[2]);
}
vec3 operator/(const vec3& v, float t) {
  auto k = 1.0f / t;
  return vec3(k * v.e_[0],
              k * v.e_[1],
              k * v.e_[2]);
}

float dot(const vec3& v1, const vec3& v2) {
  return v1.e_[0] * v2.e_[0]
         + v1.e_[1] * v2.e_[1]
         + v1.e_[2] * v2.e_[2];
}
vec3 cross(const vec3& v1, const vec3& v2) {
  return vec3(v1.e_[1] * v2.e_[2] - v1.e_[2] * v2.e_[1],
              v1.e_[2] * v2.e_[0] - v1.e_[0] * v2.e_[2],
              v1.e_[0] * v2.e_[1] - v1.e_[1] * v2.e_[0]);
}

vec3 unit_vector(const vec3& v) {
  return v / v.length();
}

#include <memory>
#include <iostream>
#include <math.h>
#include "vec3.hpp"
#include "ray.hpp"
#include "hittable.hpp"
#include "sphere.hpp"
#include "material.hpp"

sphere::sphere()
  : radius_{}
{}
sphere::sphere(const vec3& center,
               float radius,
               material* material)
  : center_(center),
    radius_(radius),
    material_(material)
{}

bool sphere::hit(const ray& r,
                 float t_min,
                 float t_max,
                 hit_record& rec) const {
  vec3 oc = r.origin() - center_;
  float a = dot(r.direction(), r.direction()),
        b = dot(oc, r.direction()),
        c = dot(oc, oc) - radius_ * radius_,
        d = b * b - a * c;
  if (d < 0) return false;
  float t = (-b - sqrt(d)) / a;
  if (t > t_min && t < t_max) {
    rec.t_ = t;
    rec.p_ = r[t];
    rec.normal_ = (rec.p_ - center_) / radius_;
    rec.material_ = material_.get();
    return true;
  }
  t = (-b + sqrt(d)) / a;
  if (t > t_min && t < t_max) {
    rec.t_ = t;
    rec.p_ = r[t];
    rec.normal_ = (rec.p_ - center_) / radius_;
    rec.material_ = material_.get();
    return true;
  }
  return false;
}
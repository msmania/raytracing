#pragma once

struct sphere : hittable {
  vec3 center_;
  float radius_;
  std::unique_ptr<material> material_;
  sphere();
  sphere(const vec3& center,
         float radius,
         material* material);
  ~sphere() {
    std::cerr << __func__ << std::endl;
  }
  virtual bool hit(const ray& r,
                   float t_min,
                   float t_max,
                   hit_record& rec) const override;
};
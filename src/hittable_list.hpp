#pragma once

struct hittable_list : hittable {
  std::vector<std::unique_ptr<hittable>> list_;
  hittable_list();
  ~hittable_list() {
    std::cerr << __func__ << std::endl;
  }
  void emplace_back(std::unique_ptr<hittable> &&item);
  virtual bool hit(const ray& r,
                   float t_min,
                   float t_max,
                   hit_record& rec) const override;
};